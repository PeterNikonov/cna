GOPRIVATE?=
GOPROXY?=

build:
	GOPROXY=${GOPROXY} GOPRIVATE=${GOPRIVATE} go mod vendor
	GOOS=linux CGO_ENABLED=0 GOPRIVATE=${GOPRIVATE} go build \
		-ldflags="-X main.gitTag=${APP_VERSION}" \
		-a -trimpath \
		-o ./dist/cna \
		./cmd

test:
	go test -coverprofile=coverage.txt -covermode count -v ./... || (gocover-cobertura < coverage.txt > /tmp/coverage.xml && exit 9)
	gocover-cobertura < coverage.txt > /tmp/coverage.xml
