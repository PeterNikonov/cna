package config

type Config struct {
	// LogLevel уровень логирования
	LogLevel string `long:"log-level" description:"Log level: panic, fatal, warn or warning, info, debug" env:"LOG_LEVEL" default:"info"`
	// ListenHttp хост и порт, который будет слушать GRPC сервер (в формате host:port)
	ListenHttp string `long:"listen-http" description:"Listening host:port for http-server" env:"LISTEN_HTTP" default:":8080"`
}
