package main

import (
	"cna/cmd/config"
	"cna/internal/controller"
	"fmt"
	"github.com/jessevdk/go-flags"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

var cfg = config.Config{}

func init() {
	parser := flags.NewParser(&cfg, flags.Default)
	parser.SubcommandsOptional = true
	_, err := parser.AddCommand("version", "Show version", "Show build version", &struct{}{})
	initError(err)
	_, err = parser.Parse()
	initError(err)
}

func main() {
	logger, err := initLogger()
	initError(err)

	defer func() {
		if msg := recover(); msg != nil {
			logger.Fatal("Panic", zap.Any("panic_err", msg))
		}
	}()

	initSignal(logger)

	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		err := serveHttp(logger)
		if err != nil {
			logger.Fatal("can't start HTTP server", zap.Error(err))
		}
		wg.Done()
	}()

	wg.Wait()
}

// initError
func initError(err error) {
	if err != nil {
		fmt.Printf("Error init: %s.\nFor help use -h\n", err)
		os.Exit(1)
	}
}

// initSignal
func initSignal(logger *zap.Logger) {
	osSigCh := make(chan os.Signal, 1)

	signal.Notify(
		osSigCh,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT,
	)

	go func() {
		s := <-osSigCh
		switch s {
		case syscall.SIGHUP:
			logger.Info("Received signal SIGHUP! Process exited")
			os.Exit(0)
		case syscall.SIGINT:
			logger.Info("Received signal SIGINT! Process exited")
			os.Exit(0)
		case syscall.SIGTERM:
			logger.Info("Received signal SIGTERM! Process exited")
			os.Exit(0)
		case syscall.SIGQUIT:
			logger.Info("Received signal SIGQUIT! Process exited")
			os.Exit(0)
		}
	}()
}

// initLogger
func initLogger() (*zap.Logger, error) {
	lvl := zap.InfoLevel

	err := lvl.UnmarshalText([]byte(cfg.LogLevel))
	if err != nil {
		return nil, fmt.Errorf("can't unmarshal log-level: %s", err)
	}

	opts := zap.NewProductionConfig()
	opts.Level = zap.NewAtomicLevelAt(lvl)
	opts.EncoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	opts.Encoding = "console"
	opts.EncoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder

	return opts.Build()
}

// serveHttp
func serveHttp(logger *zap.Logger) error {
	timeController := controller.NewTimeController()
	http.HandleFunc("/time", timeController.Time)

	if cfg.ListenHttp == "" {
		return fmt.Errorf("error --listen-http must be specified!")
	}

	logger.Info("HttpServer is started", zap.String("listen-http", cfg.ListenHttp))
	err := http.ListenAndServe(cfg.ListenHttp, nil)
	if err != nil {
		return fmt.Errorf("couldn't start HttpServer: %s", err)
	}

	return nil
}
