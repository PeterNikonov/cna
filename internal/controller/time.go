package controller

import (
	"encoding/json"
	"net/http"
	"time"
)

// TimeController контроллер запросов времени
type TimeController struct {
}

func NewTimeController() *TimeController {
	return &TimeController{}
}

// Time возвращает текущее время
func (s *TimeController) Time(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(time.Now().String())
}
