module cna

go 1.16

require (
	github.com/jessevdk/go-flags v1.5.0
	go.uber.org/zap v1.19.1
)
